const Product = require("../models/Product");
const User = require("../models/User");


// Create product
// module.exports.createProduct = (reqBody) => {

// 	let newProduct = new Product ({

// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price

// 	})

// 	return newProduct.save().then((product, error) => {

// 		if(error) {
// 			return false
// 		} else {
// 			return product
// 		}
// 	})

// }

//Testing Create product with img
module.exports.createProduct = (reqBody) => {

	let newProduct = new Product({

		name: reqBody.name,
		description: reqBody.description,
		img: reqBody.img,
		price: reqBody.price
	})

	return newProduct.save().then((product, error) => {

		if (error) {
			return false
		} else {
			return product
		}
	})

}

// Retrieve ALL ACTIVE product
// module.exports.getAllProduct = () => {

// 	return Product.find({isActive: true}).then((product, error) => {
// 		if(error) {
// 			return false
// 		} else {
// 			return product
// 		}
// 	})
// }
module.exports.getActiveProduct = () => {

	return Product.find({ isActive: true }).then((product, error) => {
		if (error) {
			return false
		} else {
			return product
		}
	})
}

// Retrieve ALL products including not active
module.exports.getAllProduct = () => {

	return Product.find({}).then((product, error) => {
		if (error) {
			return false
		} else {
			return product
		}
	})
}

// Retrieve SINGLE product
module.exports.getProduct = (reqParams) => {


	return Product.findById(reqParams).then((product, error) => {
		if (error) {
			return false
		} else {
			return product
		}

	})

}

// Updating a product
module.exports.updateProduct = (product) => {



	return Product.findById(product.productId).then((result, error) => {

		if (product.isAdmin) {
			result.name = product.updatedProduct.name
			result.description = product.updatedProduct.description
			result.image = product.updatedProduct.image // Added
			result.isActive = product.updatedProduct.isActive // Added
			result.price = product.updatedProduct.price
			console.log(result, "<----- Result after update")
			return result.save().then((updatedProduct, error) => {

				if (error) {
					return false
				} else {
					return updatedProduct
				}
			})
		} else {
			return `Need admin access`
		}
	})
}

// Archiving a product
module.exports.archiveProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		if (result.isActive) {
			result.isActive = false;
			return result.save().then((result, error) => {

				if (error) {
					return false
				} else {
					return result
				}

			})
		} else {
			return `Product already archived`
		}

	})

}