const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Route for check if email exists
module.exports.checkEmailExists = (reqBody) => {

	//The result is sent back to the frontend via the "then" method found in the route file
	return User.find({ email: reqBody.email }).then(result => {

		//the "find" method returns a record if a match is found
		if (result.length > 0) {
			return true

			//No duplicate email found
			//The user is not yet registered in the database
		} else {
			return false
		}
	})
}

// Controller for User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
		// password: reqBody.password
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false
		} else {
			return user
		}
	})
}

// Controller for Login
module.exports.loginUser = (reqBody) => {


	return User.findOne({ email: reqBody.email }).then(result => {

		if (result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result) }
			} else {
				return false;
			}
		}
	})
}


// Controller for Set as Admin
module.exports.setAdmin = (reqParams) => {

	console.log(reqParams, "<----- this is reqParams from userController")

	return User.findById(reqParams.userId).then(result => {

		console.log(result, "<----- this is result from userController")

		if (result.isAdmin === false) {

			result.isAdmin = true

			return result.save().then((result, error) => {
				if (error) {
					return false
				} else {
					return result
				}
			})
		} else {
			return `User already admin`
		}
	})
}

// Controller for create order for Non-admin
module.exports.createOrder = async (data) => {

	console.log(data);

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.purchasedItems.push({
			productId: data.productId,
			productName: data.name,
			description: data.description,
			// image: data.image, // added
			price: data.price,
			quantity: data.quantity,
			totalAmount: data.price * data.quantity
		});


		return user.save().then((user, error) => {

			if (error) {
				return false
			} else {
				return true
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.soldItems.push({
			userId: data.userId,
			price: data.price,
			quantity: data.quantity,
			totalAmount: data.price * data.quantity
		});


		return product.save().then((product, error) => {

			if (error) {
				return false
			} else {
				return true
			}

		})

	})


	if (isUserUpdated && isProductUpdated) {
		return true
	} else {
		return false
	}


}


// Retrieving ALL orders per user
// module.exports.getAllOrders = () => {

// 	return User.find({
// 		"purchasedItems": {
// 			$exists: true, $ne:[]
// 		}
// 	}).then((result, error) => {
// 		// console.log(reqBody, "<----- This is 2nd reqBody")

// 		if(error) {
// 			return false
// 		} else {
// 			return result
// 		}
// 	})
// }

// Retrieving ALL orders per product
module.exports.getAllOrders = () => {

	return Product.find({
		"soldItems": {
			$exists: true, $ne: []
		}
	}).then((result, error) => {

		if (error) {
			return false
		} else {
			return result
		}
	})
}


// Retrieve Authenticated User's Orders (NON-admin only)

module.exports.myOrders = (userData) => {

	return User.findById(userData).then((user, error) => {

		if (error) {
			return false
		} else {
			return user.purchasedItems
		}

	})

}

// Retrieve user details
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};


// Adding to cart New addition working
module.exports.addingToCart = (data) => {

	console.log(data);

	return User.findById(data.userId).then(user => {

		user.cart.push({
			productId: data.productId,
			productName: data.name,
			description: data.description, // added 04-06
			price: data.price,
			quantity: data.quantity,
			totalAmount: data.price * data.quantity
		});


		return user.save().then((user, error) => {

			if (error) {
				return false
			} else {
				return true
			}
		})
	})

}

// module.exports.addingToCart = async (data) => {

// 	console.log(data);

// 	return User.findById(data.userId).then(user => {

// 		user.cart.push(
// 			{
// 				productId: data.productId,
// 				productName: data.name,
// 				price: data.price,
// 				quantity: data.quantity,
// 				subTotalAmount: data.price * data.quantity
// 			}
// 		);


// 		return user.save().then((user, error) => {

// 			if (error) {
// 				return false
// 			} else {
// 				return true
// 			}
// 		})
// 	})

// }

// Retrieving products in cart working
module.exports.myCart = (userData) => {

	return User.findById(userData).then((user, error) => {

		if (error) {
			return false
		} else {
			return user.cart
		}

	})

}

