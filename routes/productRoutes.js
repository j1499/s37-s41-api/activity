const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");

// Route for creating a product
// router.post('/', auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);


// 	if(userData.isAdmin) {
// 		productController.createProduct(req.body).then(resultFromController => res.send(resultFromController))
// 	} else {
// 		res.send(`Need admin access`)
// 	}

// })

//Testing route for creating a product with img
router.post('/', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);


	if (userData.isAdmin) {
		productController.createProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(`Need admin access`)
	}

})

// Route for retrieving ALL active product
// router.get('/', (req, res) => {
// 	productController.getAllProduct().then(resultFromController => res.send(resultFromController))
// })
router.get('/', (req, res) => {
	productController.getActiveProduct().then(resultFromController => res.send(resultFromController))
})

// Route for retrieving ALL products including not active
router.get('/all', (req, res) => {
	productController.getAllProduct().then(resultFromController => res.send(resultFromController))
})

// Route for retrieving SINGLE product
router.get('/:productId', (req, res) => {

	productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})

// Route for updating product info (Admin only)
router.put('/:productId', auth.verify, (req, res) => {

	const product = {

		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedProduct: req.body
	}


	productController.updateProduct(product).then(resultFromController => res.send(resultFromController));

})

// Route for archiving product (admin only)
router.put('/:productId/archive', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);


	if (userData.isAdmin) {
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`Need admin access`);
	}


})


module.exports = router;