const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

//Route for checking if email exists
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for User registration
router.post('/register', (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

})

// Login for token
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for Set as admin
router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData, "<----- userData variable");

	if (userData.isAdmin) {
		userController.setAdmin(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(`Need admin access`)
	}
})

// Route for create order as Non-admin
router.post('/checkout', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData, "<----- This is the userData")

	let data = {

		userId: userData.id,
		productId: req.body.productId,
		name: req.body.name,
		description: req.body.description,
		// image: req.body.image, // added now
		price: req.body.price,
		quantity: req.body.quantity

	}
	console.log(data, "<----- This is the data")

	if (userData.isAdmin) {

		return res.send(`Not allowed to create order`)

	} else {


		userController.createOrder(data).then(resultFromController => res.send(resultFromController))

	}


})

// Route for retrieve ALL Order (Admin)
router.get('/orders', (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData, "<----- this is userData")

	if (userData.isAdmin) {

		userController.getAllOrders().then(resultFromController => res.send(resultFromController));

	} else {
		res.send(`Need admin access to retrieve orders`)
	}

})

// Route for Retrieve Authenticated User's Orders (NON-admin only)
router.get('/myOrders', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData)

	if (userData.isAdmin === false) {

		userController.myOrders(userData.id).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(`Cannot retrieve order`)
	}

})

// Route for user details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userController.getProfile({ userId: userData.id }).then(resultFromController => res.send(resultFromController));
});



// Route for adding to cart New addition working
router.post('/addToCart', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData, "<----- This is the userData")

	let data = {
		userId: userData.id,
		productId: req.body.productId,
		name: req.body.name,
		description: req.body.description, // Added 04-06
		price: req.body.price,
		quantity: req.body.quantity
	}
	console.log(data, "<----- This is the data")

	if (userData.isAdmin) {

		return res.send(`Not allowed to add to cart`)

	} else {

		userController.addingToCart(data).then(resultFromController => res.send(resultFromController))

	}
})

// router.post('/addToCart', auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);
// 	console.log(userData, "<----- This is the userData")

// 	let data = {
// 		userId: userData.id,
// 		productId: req.body.productId,
// 		price: req.body.price,
// 		quantity: req.body.quantity
// 	}
// 	console.log(data, "<----- This is the data")

// 	if (userData.isAdmin) {

// 		return res.send(`Not allowed to add to cart`)

// 	} else {

// 		userController.addingToCart(data).then(resultFromController => res.send(resultFromController))

// 	}
// })

// Route for retrieving orders in cart
router.get('/myCart', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData)

	// if(userData.isAdmin === false) {

	// 	userController.myOrders(userData.id).then(resultFromController => res.send(resultFromController));

	// } else {

	// 	res.send(`Cannot retrieve order`)
	// } 

	if (userData.isAdmin) {
		res.send(`Cannot retrieve cart`)
	} else {
		userController.myCart(userData.id).then(resultFromController => res.send(resultFromController));
	}

})

module.exports = router;