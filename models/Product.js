const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name required"]
	},
	description: {
		type: String,
		required: [true, "Description required"]
	},
	price: {
		type: Number,
		required: [true, "Price required"]
	},
	//Added
	img: {
		type: String,
		required: [true, "img required"]
	},
	// Added
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date() 
	},
	soldItems: [
		{
			userId: {
				type: String,
				required: [true, "userId required"]
			},
			price: {
				type: Number,
				required: [true, "price required"]
			},
			quantity : {
				type: Number,
				required: [true, "price required"]
			},
			totalAmount: {
				type: Number,
				required: [true, "total required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);