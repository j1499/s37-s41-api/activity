const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "Firstname required"]
	},
	lastName: {
		type: String,
		required: [true, "Lastname required"]
	},
	email: {
		type: String,
		required: [true, "Email required"]
	},
	password: {
		type: String,
		required: [true, "Password required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	// New addition working
	cart: [
		{
			productId: {
				type: String,
				required: [true, "Cart - ProductId required"]
			},
			productName: {
				type: String,
				required: [true, "Cart - productName required"]
			},
			description: {
				type: String,
				required: [true, "Cart - description required"]
			}, // Added 04-06
			price: {
				type: Number,
				required: [true, "Cart - price required"]
			},
			quantity: {
				type: Number,
				required: [true, "Cart - qty required"]
			},
			totalAmount: {
				type: Number,
				required: [true, "Cart - total required"]
			},
			addedOn: {
				type: Date,
				default: new Date()
			}
		}
	],
	// New addition working
	// cart: [
	// 	{
	// 		productId: {
	// 			type: String,
	// 			required: [true, "Cart - ProductId required"]
	// 		},
	// 		price: {
	// 			type: Number,
	// 			required: [true, "Cart - price required"]
	// 		},
	// 		quantity: {
	// 			type: Number,
	// 			required: [true, "Cart - qty required"]
	// 		},
	// 		subTotalAmount: {
	// 			type: Number,
	// 			required: [true, "Cart - total required"]
	// 		},
	// 		addedOn: {
	// 			type: Date,
	// 			default: new Date()
	// 		}
	// 	},
	// 	{
	// 		totalAmount: {
	// 			type: Number,
	// 			required: true
	// 		}
	// 	}
	// ],
	purchasedItems: [
		{
			productId: {
				type: String,
				required: [true, "productId required"]
			},
			productName: {
				type: String,
				required: [true, "productName required"]
			},
			description: {
				type: String,
				required: true
			},
			// image: {
			// 	type: String,
			// 	required: [true, "image required"]
			// }, // added
			price: {
				type: Number,
				required: [true, "price required"]
			},
			quantity: {
				type: Number,
				required: [true, "price required"]
			},
			totalAmount: {
				type: Number,
				required: [true, "total required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);